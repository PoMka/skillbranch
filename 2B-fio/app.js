//express
var express = require("express");
var app= express();

function correctSurname (word){
    return word.toLowerCase()[0].toUpperCase() + word.toLowerCase().slice(1);
}

function correctNames (word) {
    return word.slice(0,1).toUpperCase() + ".";
}

function fioCanonize (fio) {
   //clean fio
   //from spaces in the start of line
   fio = fio.replace(/^\s+/g,"");
   //from the additional spaces in the middle
   var cleanFio = fio.replace(/\s+/g," ");
   
   //split on different words based on space
   var splitFio = cleanFio.split(" ");
   
   //check correct fio without digits and other simbols
   var check = 0;
   splitFio.forEach(function(fioPart){
    //   console.log(fioPart.match(/([0-9\.\,\-\_\/\)\(\!\|\~\*\"]+)/));
    //   if(fioPart.match(/([^\sa-zA-Zа-яА-ЯЁё']+)/) == null){
      if(fioPart.match(/([0-9\.\,\-\_\/\)\(\!\|\~\*\"]+)/) == null){
           check += 1;
       }
       return check;
   });
   
   //some vars
   var fioResult;
   
   //cases based on amount of provided words
   //check what type of fullname provided
   if (check === splitFio.length){
       switch (splitFio.length) {
           //just Family Name
            case 1:
                fioResult = correctSurname(splitFio[0]);
                break;
            //Name and Family Name
            case 2:
                fioResult = correctSurname(splitFio[1]) + " " + correctNames(splitFio[0]);
                break;
            //fullname
            case 3:
                fioResult = correctSurname(splitFio[2]) + " " + correctNames(splitFio[0]) + " " + correctNames(splitFio[1]);
                break;
            //any other - invalid
            default:
                fioResult = "Invalid fullname";
       }
   } else {
       fioResult = "Invalid fullname";
   }
   return fioResult;
}

//EXPRESS
//CORS
app.use(function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

var i = 0;
//root route
app.get('/', function (req, res){
   var fullname = req.query.fullname;
   //check that fullname is not space or empty
   if(fullname === " "| fullname === ""){
       res.send("Invalid fullname");
   } else {
       var reply = fioCanonize(fullname);
       res.send(reply.toString()); 
   }
});

//Server start
app.listen(process.env.PORT, process.env.IP, function (){
    console.log("FIO app server started!");
})