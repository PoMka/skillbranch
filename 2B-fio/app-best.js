//BEST PRACTICE FROM OTHER USERS RESULTS

//express
// import express from 'express';
const express = require("express");
const app = express();

function firstBigThenSmall (word){
    return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase(); //new added - charAt()
}

function shortNames (word) {
    return word.slice(0,1) + ".";
}

function fioCanonize (fio) {
   //clean fio
   //from spaces on the sides and in the middle
   const cleanFio = fio.trim().replace(/\s+/g," "); //new added - function trim()
   
   //split on different words based on space
   //and bring to correct writing
   const splitFio = cleanFio.split(" ")
                    .map(word => {return firstBigThenSmall(word)}) //new added - function map()
                    .reverse(); //reverse to get surname first
   
   //add names to data in splitFIO array                 
   const[surname, secondName, thirdName] = splitFio;
   
   //check correct fio without digits and other simbols
   let check = 0;
   splitFio.forEach((fioPart) => {
    //   if(fioPart.match(/([^\sa-zA-Zа-яА-ЯЁё']+)/) == null){
      if(fioPart.match(/([0-9\.\,\-\_\/\)\(\!\|\~\*\"]+)/) == null){
           check += 1;
       }
       return check;
   });
   
   //some var for result
   let fioResult;
   
   //cases based on amount of provided words
   //check what type of fullname provided
   if (check === splitFio.length){
       switch (splitFio.length) {
           //just Family Name
            case 1:
                fioResult = surname;
                break;
            //Name and Family Name
            case 2:
                fioResult = surname + " " + shortNames(secondName);
                break;
            //fullname
            case 3:
                fioResult = surname + " " + shortNames(thirdName) + " " + shortNames(secondName);
                break;
            //any other - invalid
            default:
                fioResult = "Invalid fullname";
       }
   } else {
       fioResult = "Invalid fullname";
   }
   
   return fioResult;
}

//EXPRESS
//CORS
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//root route
app.get('/', (req, res) => {
   const fullname = req.query.fullname;
   //check that fullname is not space or empty
   if(fullname === " "| fullname === ""){
       res.send("Invalid fullname");
   } else {
       const reply = fioCanonize(fullname);
       res.send(reply.toString()); 
   }
});

//Server start
app.listen(process.env.PORT, process.env.IP, () => {
    console.log("FIO app server started!");
})