//express
var express = require("express");
var app= express();

//check if provided data is Intenger or return 0
function toInt (data) {
   var result;
   if(parseInt(data, 10)) {
      result = parseInt(data, 10);
   } else {
      result = 0;
   }
   return result;
}
//CORS
app.use(function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//root route
app.get('/', function (req, res){
   var a = toInt(req.query.a);
   var b = toInt(req.query.b);
   
   var response = a + b;
   res.send(response.toString()); 
});

//Server start
app.listen(process.env.PORT, process.env.IP, function (){
    console.log("App server started!");
})