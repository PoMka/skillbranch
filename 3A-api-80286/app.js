// express
const express = require('express');

const fetch = require('isomorphic-fetch');

const app = express();

// CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

// get sructure
const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

let pc = {};
fetch(pcUrl)
    .then(async (res) => {
      pc = await res.json();
    })
    .catch((err) => {
      console.log('Что-то пошло не так:', err);
    });


// ROUTES
// get all model
app.get('/', async (req, res) => {
  await res.json(pc);
});

// get volumes
app.get('/volumes', async (req, res) => {
  console.log('===request====');
  try {
    const hdd = pc.hdd;
    const driveSize = [];

    hdd.forEach((el) => {
            // console.log('from HDD goes drive ' + el['volume']); //debug
      if (driveSize[0]) {
                // console.log('driveSize array is NOT empty. Let\'s check what we have...'); //debug
                // console.log(driveSize);  //debug

        for (let i = 0; i < driveSize.length; i++) {
                    // если в массиве есть данные - взять и проверить, какая буква
          if (driveSize[i].letter === el.volume) {
                            // console.log('Drive ' + el['volume'] + ' is already exists in driveSize array. Need to increase total ' + driveSize[i].letter + ' size in driveSize array...');  //debug
            driveSize[i].space = driveSize[i].space + el.size;
                            // console.log(driveSize[i].letter + ' size increased');  //debug
            break;
          } else {
                            // console.log('No drive ' + el['volume'] + ' in driveSize array. Let\'s add. Creating...');  //debug

            const oneDrive = {};

            oneDrive.letter = el.volume;
            oneDrive.space = el.size;
            driveSize.push(oneDrive);
                            // console.log('created drive: ' + oneDrive.letter + ' and added to driveSize array.');  //debug
            break;
          }
        }
      } else {
                // console.log('no such drive exists. Creating...');  //debug
        const oneDrive = {};

        oneDrive.letter = el.volume;
        oneDrive.space = el.size;
        driveSize.push(oneDrive);
                // console.log('created drive ' + oneDrive.letter + ' and added to driveSize array.');  //debug
      }
    });

        // console.log('final ===================');  //debug
        // console.log(driveSize);  //debug

        // need to convert format: 'drive:':'size+B'
    const newObj = {};
    driveSize.forEach((el) => {
      const key = el.letter;
      const value = `${el.space}B`;

      newObj[key] = value;
    });

    await res.json(newObj);
  } catch (err) {
    console.log(err);
  }
});

// get object
app.get('/:key', async (req, res) => {
  const objKey = req.params.key.toString();
  const reply = pc[objKey];

  (reply !== undefined) ? await res.json(reply) : await res.sendStatus(404);

  console.log(`${objKey} - ${reply}`);
});

// get field
app.get('/:key/:value', async (req, res) => {
  const objKey = req.params.key;

  let objValue;
  (req.params.value === 'length') ? await res.sendStatus(404) : objValue = req.params.value;

  console.log(`${objKey}/${objValue}`);

  (pc[objKey] === undefined || pc[objKey][objValue] === undefined) ? await res.sendStatus(404) : await res.json(pc[objKey][objValue]);
});

// get field of field
app.get('/:key/:value/:addValue', async (req, res) => {
  const objKey = req.params.key;
  const objValue = req.params.value;

  let additionalValue;
  (req.params.addValue === 'length') ? await res.sendStatus(404) : additionalValue = req.params.addValue;

  console.log(`${objKey}/${objValue}/${additionalValue}`);

  (pc[objKey] === undefined || pc[objKey][objValue] === undefined || pc[objKey][objValue][additionalValue] === undefined) ? await res.sendStatus(404) : await res.json(pc[objKey][objValue][additionalValue]);
});

// Server start
app.listen(process.env.PORT, process.env.IP, () => {
  console.log('API-80286 app server started!');
});
