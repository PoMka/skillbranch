// express
const express = require('express');

const fetch = require('isomorphic-fetch');

const app = express();

// CORS
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

// get sructure
const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

let pc = {};
fetch(pcUrl)
    .then(async (res) => {
      pc = await res.json();
    })
    .catch((err) => {
      console.log('Что-то пошло не так:', err);
    });


// ROUTES
// get all model
app.get('/', async (req, res) => {
  await res.json(pc);
});

// get volumes
app.get('/volumes', async (req, res) => {
  const volumes = {};
  pc.hdd.forEach((el) => {
    if (volumes[el.volume]) volumes[el.volume] += el.size; // if key in object with this el.volume alredy exists - just add size
    else volumes[el.volume] = el.size; // else - add key-value to object
  });
  for (i in volumes) volumes[i] += 'B'; // need to add 'B' for reply

  res.json(volumes);
});

// get object
app.get('/:key', async (req, res) => {
  const objKey = req.params.key.toString();

  (pc[objKey] === undefined) ? await res.sendStatus(404) : await res.json(pc[objKey]);

  console.log(`${objKey} - ${pc[objKey]}`);
});

// get field
app.get('/:key/:value', async (req, res) => {
  const objKey = req.params.key;
  const objValue = req.params.value;

  (req.params.value === 'length')
        ? await res.sendStatus(404)
        : (pc[objKey] === undefined || pc[objKey][objValue] === undefined)
            ? await res.sendStatus(404)
            : await res.json(pc[objKey][objValue]);

  console.log(`${objKey}/${objValue}`);
});

// get field of field
app.get('/:key/:value/:addValue', async (req, res) => {
  const objKey = req.params.key;
  const objValue = req.params.value;
  const additionalValue = req.params.addValue;

  (req.params.addValue === 'length')
        ? await res.sendStatus(404)
        : (pc[objKey] === undefined || pc[objKey][objValue] === undefined || pc[objKey][objValue][additionalValue] === undefined)
            ? await res.sendStatus(404)
            : await res.json(pc[objKey][objValue][additionalValue]);

  console.log(`${objKey}/${objValue}/${additionalValue}`);
});

// Server start
app.listen(process.env.PORT, process.env.IP, () => {
  console.log('API-80286 app server started!');
});
