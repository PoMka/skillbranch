function canonize(url) {
  //work with http and https and without
  //work with text starts from @ or with just username
  //work with com.uk or similar double domain zones
  //work with subdomains and without
  //work with usernames with dot, underscore, dash in it
  //work with usernames with digits in it
  const re = new RegExp('(?:https?:(?=//))?(?:\/\/)?(?:\@)?(?:[a-z0-9\-]*[\.][a-z0-9\-]*(?=[\/\.]))?(?:[a-z0-9]*[\.][a-z0-9]*(?=[\/\.]))?(?:[a-z0-9]*[\.][a-z0-9]*(?=[\/\.]))?(?:\/)?(?:\@)?([a-z0-9]*[\.\_\-]?[a-z0-9]*)', 'i');
  const username = url.match(re);
  return '@' + username[1];
}

module.exports = canonize;