const data =[

  'subdomain.domain.com.uk/igorsuvorov',
  'subdomain.domain.com.uk/igor2_suvorov',
  'subdomain.domain.com.uk/igor3-suvorov',
  'subdomain.domain.com.uk/igor4suvorov',
  'subdomain.domain.com.uk/igor5.suvorov',

  'subdomain.domain.com/igorsuvorov',
  'subdomain.domain.com/igor2_suvorov',
  'subdomain.domain.com/igor3-suvorov',
  'subdomain.domain.com/igor4suvorov',
  'subdomain.domain.com/igor5.suvorov',

  'https://subdomain.domain.com.uk/igorsuvorov',
  'https://subdomain.domain.com.uk/igor2_suvorov',
  'https://subdomain.domain.com.uk/igor3-suvorov',
  'https://subdomain.domain.com.uk/igor4suvorov',
  'https://subdomain.domain.com.uk/igor5.suvorov',

  'http://subdomain.domain.com.uk/igorsuvorov',
  'http://subdomain.domain.com.uk/igor2_suvorov',
  'http://subdomain.domain.com.uk/igor3-suvorov',
  'http://subdomain.domain.com.uk/igor4suvorov',
  'http://subdomain.domain.com.uk/igor5.suvorov',

  '//subdomain.domain.com.uk/igorsuvorov',
  '//subdomain.domain.com.uk/igor2_suvorov',
  '//subdomain.domain.com.uk/igor3-suvorov',
  '//subdomain.domain.com.uk/igor4suvorov',
  '//subdomain.domain.com.uk/igor5.suvorov',

  'domain.com.uk/igorsuvorov',
  'domain.com.uk/igor2_suvorov',
  'domain.com.uk/igor3-suvorov',
  'domain.com.uk/igor4suvorov',
  'domain.com.uk/igor5.suvorov',

  'https://domain.com.uk/igorsuvorov',
  'https://domain.com.uk/igor2_suvorov',
  'https://domain.com.uk/igor3-suvorov',
  'https://domain.com.uk/igor4suvorov',
  'http://domain.com.uk/igorsuvorov',
  'https://domain.com.uk/igor5.suvorov',

  'http://domain.com.uk/igor2_suvorov',
  'http://domain.com.uk/igor3-suvorov',
  'http://domain.com.uk/igor4suvorov',
  'http://domain.com.uk/igor5.suvorov',

  'domain.com/igorsuvorov',
  'domain.com/igor2_suvorov',
  'domain.com/igor3-suvorov',
  'domain.com/igor4suvorov',
  'domain.com/igor5.suvorov',

  '//domain.com/igorsuvorov',
  '//domain.com/igor2_suvorov',
  '//domain.com/igor3-suvorov',
  '//domain.com/igor4suvorov',
  '//domain.com/igor5.suvorov',

  'https://domain.com/igorsuvorov',
  'https://domain.com/igor2_suvorov',
  'https://domain.com/igor3-suvorov',
  'https://domain.com/igor4suvorov',
  'https://domain.com/igor5.suvorov',
  
  'https://domain.com/igorsuvorov?ver=1&acc=24jfhs32e20d',
  'https://domain.com/igor2_suvorov?ver=1&acc=24jfhs32e20d',
  'https://domain.com/igor3-suvorov?ver=1&acc=24jfhs32e20d',
  'https://domain.com/igor4suvorov?ver=1&acc=24jfhs32e20d',
  'https://domain.com/igor5.suvorov?ver=1&acc=24jfhs32e20d',

  'http://domain.com/igorsuvorov',
  'http://domain.com/igor2_suvorov',
  'http://domain.com/igor3-suvorov',
  'http://domain.com/igor4suvorov',
  'http://domain.com/igor5.suvorov',

  '@skillbranch',
  '@skill93branch',
  '@skill.branch',
  '@skill_branch',
  '@skill-branch',

  'skillbranch',
  'skillbranch93',
  'skill.branch',
  'skill_branch',
  'skill-branch',

  'httpdomain.com/igorsuvorov',
  'httpdomain.com/igor2_suvorov',
  'httpdomain.com/igor3-suvorov',
  'httpdomain.com/igor4suvorov',
  'httpdomain.com/igor5.suvorov',
  
  'xn--e1afmkfd.xn--80akhbyknj4f/igor1.suvorov',
  'xn--e1afmkfd.xn--80akhbyknj4f/igor2-suvorov',
  'xn--e1afmkfd.xn--80akhbyknj4f/igor3_suvorov',
  'xn--e1afmkfd.xn--80akhbyknj4f/igor5suvorov',
  
  'вконтакте.рф/igor1.suvorov',
  'вконтакте.рф/igor2-suvorov',
  'вконтакте.рф/igor3_suvorov',
  'вконтакте.рф/igor5suvorov',


];