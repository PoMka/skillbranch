const express = require('express');
const cors = require('cors');
const app = express();

app.use(cors());

const canonize =require('./canonize');

//CORS
// app.use(function(req, res, next){
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//     res.header("Access-Control-Allow-Methoda", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
//     next();
// });

// function allowAccess(req, res, next) {
//   if(req.method === 'OPTIONS') {
//     res.sendStatus(200);
//   } else {
//     next();
//   }
// }

// app.use(allowAccess);

// app.options('/', (req, res) => {
//   console.log(req.headers);
//   res.sebdStatus(200);
// });

let i = 1;
app.get('/', (req, res) => {
  // console.log(req.headers);
  const username = decodeURI(req.query.username);
  console.log(i + ") " + username + " - " + req.headers.origin);
  const result = canonize(username);
  i += 1;
  res.send(result);
});

// app.listen(3000, () => {
//   console.log('App server is listening on port 3000...');
// });
//Server start
app.listen(process.env.PORT, process.env.IP, () => {
    console.log("App server started!");
});

//see testdata.js for all possible variation of input data
