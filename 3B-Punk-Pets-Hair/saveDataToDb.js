const Pet = require('./models/pet');
const User = require('./models/user');
// const Promise = require('bluebird');

function getOneUser (param) {
  return User.findOne({'id': param}, function (err, doc) {
        console.log('Ошибка: ' + err);
        // return doc['_id'];
      })['_id'];
};


async function saveDataToDb(data) {
  try {
    //clear DB
    // User.remove({}, (err) => {
    //   (err) ? console.log(err) : await console.log("Users removed");
    // });    
    // Pet.remove({}, (err) => {
    //   (err) ? console.log(err) : await console.log("Pets removed");
    // });
    //lets add all users
    //get one from array
    const users = data.users.map( (user) => {
      const userData = Object.assign({}, user);
      return (new User(userData)).save();
    });
    
    const allUsers = await Promise.all(users);
    
    //lets add Pets to Users
    //get one from array
    const promisesPets = data.pets.map( (pet) => {
      //need to get _id for User of this pet
      const userId = getOneUser (pet.userId);
      console.log('userId: ' + userId);
      //add to DB
      const petData = Object.assign({}, pet, {
        owner: userId
      });
      return (new Pet(petData)).save();
    });

    const allPets = await Promise.all(promisesPets);

    console.log('Data was added successfully');
    


    // return {
    //   users,
    //   pets: await Promise.all(promises)
    // };

  } catch (err) {
    console.log('Ошибка: ', err);
    throw err;
  }
};

module.exports = saveDataToDb;
