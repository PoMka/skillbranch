var mongoose = require("mongoose");
//const _ = require('lodash');

//schema setup
var userSchema = new mongoose.Schema({
  id: {
    type: Number,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  fullname: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  values: [{
    money: String,
    origin: String
     }],
  },{
    timestamps: true,
  }
);


//userSchema.methods.toJSON = function () {
//    return _.pick(this, ['name']);
//};

var User = mongoose.model("User", userSchema);
module.exports = User;
