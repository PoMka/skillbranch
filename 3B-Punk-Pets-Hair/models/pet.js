var mongoose = require("mongoose");
//const _ = require('lodash');

//schema setup
var petSchema = new mongoose.Schema({
  id: {
    type: Number,
    required: true
  },
  userId: {
    type: Number,
    required: true
  },
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  type: {
    type: String,
    //        enum: ['cat', 'dog'],
    required: true
  },
  color: {
    type: String,
    required: true
  },
  age: {
    type: Number,
    required: true
  }

}, {
  timestamps: true,
});

//petSchema.methods.toJSON = function () {
//  return _.pick(this, ['name', 'type', 'owner']);
//};


var Pet = mongoose.model("Pet", petSchema);
module.exports = Pet;
