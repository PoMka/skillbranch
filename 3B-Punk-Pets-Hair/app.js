// localhost
// const port = '3000';
// const host = 'localhost';
// c9.io
const port = process.env.PORT;
const host = process.env.IP;

const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const fetch = require('isomorphic-fetch');
const Promise = require('bluebird');
const _ = require('lodash');
// data models
const Pet = require('./models/pet');
const User = require('./models/user');
const saveDataToDb = require('./saveDataToDb');

const app = express();
app.use(cors());

// mongoose.connect('mongodb://petshair:12345@ds047365.mlab.com:47365/3b-pets');
mongoose.connect('mongodb://localhost/3b-pets');
mongoose.Promise = global.Promise;

// Get data from URL and add it to DB
const dataUrl = 'https://gist.githubusercontent.com/isuvorov/55f38b82ce263836dadc0503845db4da/raw/pets.json';

fetch(dataUrl)
  .then(async (res) => {
    const data = await res.json();
    saveDataToDb(data);
  })
  .catch((err) => {
    console.log('Ошибка тут: ', err);
  });


// Server start
app.listen(port, host, () => {
  console.log('Punk Heir Pets app server started!');
});
