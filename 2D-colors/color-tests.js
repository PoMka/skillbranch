const data =[

  'f',
  'ff',
  'fFf',
  'ffFf',
  'FFfff',
  'FFfffF',
  '%20fff',
  '#f',
  '#ff',
  '%20fff',
  '%20ff%20f%20',
  '%23fffdd',
  ' #fff',
  '-fff',
  'xfile',
  ' abc',
  '#-123',
  'rgb(255,128,128)',
  ' rgb( 0, 255 , 64 )',
  'rgb( 128 , 128 , 128)',
  
];
  // '#rgb(128,128,128)',
  // '%23rgb(128 , 128,128)',
  // '-rgb(128,128,257)',
  // 'rgb(255,128,257)',
  // 'rgba(128,128,255)',
  // 'rgba(128,128,258)',
  // '#-rgb(128,128,128)',
  // 'hsl(0,10,10)',
  // 'hsl(250,10,120,1)',
  // 'hsl(360,10,10)',
  // 'hsl(361,10,10)',
  // 'hsl(360,120,10)',
  // '-hsl(20,10,10,1)',
  // 'hsl(20,10,10,1)',
  // '#hsl(20,10,10,1)',
  // '#-hsl(20,10,10,1)',

// ];


// module.exports = data;

// data.forEach( (color) => {
//   const result = getHex(color);
  
//   console.log(color + ' - ' + result);
// });


function getHex (color) {
  //clean query request
  // let result = decodeURI(color).toLowerCase().replace(/\s+/g, '');
  let result;
  if(color) {
    result = color.toLowerCase().replace(/\s+|%20|%23/g, '');
  } else {
    return result = 'Invalid color';
  }

  //invalid #rgb(...) and #hsl(...)
  ((/^#-?rgba?/i).test(result)) ? result = 'Invalid color' : 0;
  ((/^#-?hsla?/i).test(result)) ? result = 'Invalid color' : 0;
  
  //clean # if exists
  (result.charAt(0) ==='#') ? result = result.slice(1) : result;
  
  //invalid -(...)
  (result.charAt(0) ==='-') ? result = 'Invalid color' : 0;

  //can be 3hex, 5hex, 6hex -> to full hex
  if (result.length === 3 || result.length === 5 || result.length === 6) {
    result = correctHex(result);
  } else if (result.slice(0, 3) === 'rgb') {
    result = rgbToHex(result);
  } else if (result.slice(0, 3) === 'hsl') {
    const rgb = hslToRgb(result);
    (rgb === 'Invalid color') ? result = 'Invalid color' : result = rgbToHex(rgb);
  } else {
    result = 'Invalid color';
  }
  
  return result;
}


//check hex or not
function isHex(color) {
const res = (/^[0-9a-f]{3}(?:[0-9a-f]{2,3})?$/i).test(color);
return res;
}

//short hex to full hex
function correctHex(color) {
  let newHex = [];
  let res;
  
  if (color.length === 3 && isHex(color)) {
    newHex[0] = color.charAt(0);
    newHex[1] = color.charAt(0);
    newHex[2] = color.charAt(1);
    newHex[3] = color.charAt(1);
    newHex[4] = color.charAt(2);
    newHex[5] = color.charAt(2);
  
    res = newHex.join('');
    
  } else if (color.length === 5 && isHex(color)) {
    newHex[0] = color.charAt(0);
    newHex[1] = color.charAt(1);
    newHex[2] = color.charAt(2);
    newHex[3] = color.charAt(3);
    newHex[4] = 0;
    newHex[5] = color.charAt(4);
  
    res = newHex.join('');
    
  } else if (isHex(color)) {
    res = color;
  
  } else {
    res = 'Invalid color';
    
  }
  return res;
}

//RGB to HEX
function rgbToHex(color) {
  let res;
  let rgb;
  if (color.split(',').length > 3) {
    return res = 'Invalid color';
  } else {
    rgb = color.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
  }
  res = (rgb && rgb.length === 4 && rgb[1] <= 255  && rgb[2] <= 255 && rgb[3] <= 255) ?
    ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
    ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
    ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : res = 'Invalid color';
  return res;
}

//HSL to RGB
function hslToRgb (color) {
  let hsl, r, g, b, m, c, x, res;
  if (!color.match(/^[hsla?(]+(\d+),(\d+)%,(\d+)%/i)) {
    return res = "Invalid color";
  } else {
    hsl = color.match(/^[hsla?(]+(\d+),(\d+)%?,(\d+)%?/i);
  }

  if (hsl[1] > 360 || hsl[2] > 100 || hsl[3] > 100) {
    return res = 'Invalid color'; 
  }

  if (!isFinite(hsl[1])) hsl[1] = 0;
  if (!isFinite(hsl[2])) hsl[2] = 0;
  if (!isFinite(hsl[3])) hsl[3] = 0;
  
  hsl[1] /= 60;
  if (hsl[1] < 0) hsl[1] = 6 - (-hsl[1] % 6);
  hsl[1] %= 6;

  hsl[2] = Math.max(0, Math.min(1, hsl[2] / 100));
  hsl[3] = Math.max(0, Math.min(1, hsl[3] / 100));

  c = (1 - Math.abs((2 * hsl[3]) - 1)) * hsl[2];
  x = c * (1 - Math.abs((hsl[1] % 2) - 1));

  if (hsl[1] < 1) {
      r = c;
      g = x;
      b = 0;
  } else if (hsl[1] < 2) {
      r = x;
      g = c;
      b = 0;
  } else if (hsl[1] < 3) {
      r = 0;
      g = c;
      b = x;
  } else if (hsl[1] < 4) {
      r = 0;
      g = x;
      b = c;
  } else if (hsl[1] < 5) {
      r = x;
      g = 0;
      b = c;
  } else {
      r = c;
      g = 0;
      b = x;
  }

  m = hsl[3] - c / 2;
  r = Math.round((r + m) * 255);
  g = Math.round((g + m) * 255);
  b = Math.round((b + m) * 255);
  
  res = 'rgb(' + r + ',' + g + ',' + b + ')';
  return res;

}

module.exports = getHex;
