//express
const express = require('express');
const cors = require('cors');

//ENV settings
// const port = '3000';
// const host = 'localhost';
const port = process.env.PORT;
const host = process.env.IP;

//modules
const getHex = require('./color-tests');

const app = express();
app.use(cors());

app.get('/', async(req, res) => {
  const color = req.query.color;
  let result = getHex(color);

  (result === 'Invalid color') ? result : result = '#' + result;
  
  //lets see what we get in...
  console.log(color + ' - ' + result);
  
  res.send(result);
});


app.listen(port, host, () => {
  console.log('2D-Colors app server started!');
});
