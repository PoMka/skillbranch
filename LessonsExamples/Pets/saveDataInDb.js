
const Pet = require("./models/pet");
const User = require("./models/user");

// const saveDataInDb = 
async function saveDataInDb (data) {
    try {
        const user = new User(data.user);
        await user.save();
        
        const promises = data.pets.map( (pet) => {
            const petData = Object.assign({}, pet, {
                owner: user._id
            });
            return (new Pet(petData)).save();
        });
        
        console.log('Data was added successfully');
        
        return {
            user, 
            pets: await Promise.all(promises)
        };
            
    } catch (err) {
        console.log('err', err);
        throw err;
    }
};

module.exports = saveDataInDb;