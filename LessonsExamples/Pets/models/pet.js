var mongoose = require("mongoose");
const _ = require('lodash');

//schema setup
var petSchema = new mongoose.Schema({
    type: {
        type: String,
        enum: ['cat', 'dog'],
        required: true
    },
    name: {
        type: String,
        required: true
    },
    owner:{
            type: mongoose.Schema.Types.ObjectId,
            ref: "User",
            required: true
          },
    },{
    timestamps: true,
});

petSchema.methods.toJSON = function () {
    return _.pick(this, ['name', 'type', 'owner']);
};


var Pet = mongoose.model("Pet", petSchema);
module.exports = Pet;