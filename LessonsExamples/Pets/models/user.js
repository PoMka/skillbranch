var mongoose = require("mongoose");
const _ = require('lodash');

//schema setup
var userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
        }
    }, {
    timestamps: true,
});


userSchema.methods.toJSON = function () {
    return _.pick(this, ['name']);
};

var User = mongoose.model("User", userSchema);
module.exports = User;