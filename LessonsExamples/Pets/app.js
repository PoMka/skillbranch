// localhost
const port = '3000';
const host = 'localhost';
// c9.io
//const port = process.env.PORT;
//const host = process.env.IP;

const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const Promise = require('bluebird');
const _ = require('lodash');

const app = express();
app.use(bodyParser.json());
mongoose.connect("mongodb://petsapp:4381352@ds159217.mlab.com:59217/pets-app");

mongoose.Promise = global.Promise;

const Pet = require("./models/pet");
const User = require("./models/user");
const saveDataInDb = require('./saveDataInDb');
const isAdmin = require('./middlewares/isAdmin');

const data = {
    user: {
        name: "rdubovik",
    },
    pets: [
        {
            name: "Cat1",
            type: "cat"
        },
        {
            name: "Dog1",
            type: "dog"
        }
    ]
};

//saveDataInDb(data);

//CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.get('/users', async(req, res) => {
    const users = await User.find();
    res.json(users);
});

app.get('/pets', async(req, res) => {
    const pets = await Pet.find().populate('owner');
    res.json(pets);
});

app.get('/clear', isAdmin, async(req, res) => {
    await User.remove({});
    await Pet.remove({});
    return res.json('OK. All data removed from DB');
});

app.post('/data', async(req, res) => {
    const data = req.body;
    (!data.user) ? res.status(400).send('user required'): 0;
    (!data.pets) ? data.pets = []: 0;

    const user = await User.findOne({
        name: data.user.name
    });
    (user) ? res.status(400).send('user.name is exists'): 0;

    try {
        const result = await saveDataInDb(data);
        return res.json(result);
    } catch (err) {
        return res.status(500).json(err);
    }
});

//Server start
app.listen(port, host, () => {
    console.log("Pets app server started!");
})
