// localhost
//const port = '3000';
//const host = 'localhost';
// c9.io
const port = process.env.PORT;
const host = process.env.IP;

const express = require('express');
const fetch = require('isomorphic-fetch');
const Promise = require('bluebird');
const _ = require('lodash');

const app = express();

const baseUrl = 'http://pokeapi.co/api/v2';
const pokemonFields = ['id', 'name', 'base_experience', 'height', 'weight'];

async function getPokemons(url, i = 1) {
    const response = await fetch(url);
    const page = await response.json();
    const pokemons = page.results;

    if (i > 2) {
        return pokemons;
    }
    console.log('====== round ' + i + ' ======');
    console.log('Looking for ' + url);

    if (page.next) {
        const pokemons2 = await getPokemons(page.next, i + 1);
        return [
                ...pokemons,
                ...pokemons2
            ];
    }

    return pokemons;
};

async function getPokemon(url) {
    console.log('==== need more info ====');
    console.log('Getting info for pokemon on url: ' + url);
    const response = await fetch(url);
    const pokemon = await response.json();
    return pokemon;
};


app.get('/', async(req, res) => {
    try {
        const pokemonsUrl = `${baseUrl}/pokemon`;
        //get list of pokemons
        const pokemonsInfo = await getPokemons(pokemonsUrl);
        // const pokemonsPromises = pokemonsInfo.slice(0, 2).map( el => {
        //get info from list about each pokemon in it
        const pokemonsPromises = pokemonsInfo.map(el => {
            return getPokemon(el.url);
        });
        const pokemonsFull = await Promise.all(pokemonsPromises);
        //pick in new Array only necessary fields
        const pokemons = pokemonsFull.map(el => {
            return _.pick(el, pokemonFields);
        });
        //sorting pokemons by weight from big to small
        const sortedPokemons = _.sortBy(pokemons, (el) => -el.weight);

        console.log('===== done =====');
        return res.json(sortedPokemons);

    } catch (err) {
        console.log('error: ' + err);
    }
});


//Server start
app.listen(port, host, () => {
    console.log("Pokemons app server started!");
})
